//
//  LocationTableViewCell.swift
//  AssignmentProject
//
//  Created by webwerks on 10/07/19.
//  Copyright © 2019 webwerks. All rights reserved.
//

import UIKit

class LocationTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLBL: UILabel!
    @IBOutlet weak var longitudeLBL: UILabel!
    @IBOutlet weak var latitudeLBL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
