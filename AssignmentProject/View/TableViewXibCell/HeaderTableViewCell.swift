//
//  HeaderTableViewCell.swift
//  AssignmentProject
//
//  Created by webwerks on 10/07/19.
//  Copyright © 2019 webwerks. All rights reserved.
//

import UIKit

class HeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var infoHeaderLBL: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
