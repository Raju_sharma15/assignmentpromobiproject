//
//  ViewController.swift
//  AssignmentProject
//
//  Created by webwerks on 08/07/19.
//  Copyright © 2019 webwerks. All rights reserved.
//

import UIKit


class HomeviewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var locationModelArray = [LocationInfoModel]()
    var deviceInfoModelArray = [DeviceInfoModel]()
    var deviceInfoModel = DeviceInfoViewModel()
    var headertitleArray = [String]()
    var key256   = "12345678901234561234567890123456"   // 32 bytes for AES256
    var iv       = "abcdefghijklmnop"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Information Detail "
        self.headertitleArray = ["Location detail","Device last info",]
        self.setupXIB()
        deviceInfoModel.intializeMethod()
        deviceInfoModel.locationArray =  { locationArray in
            self.locationModelArray = locationArray
            self.tableView.reloadData()
        }
        deviceInfoModel.deviceArray = { deviceArray in
              self.deviceInfoModelArray = deviceArray
             self.tableView.reloadData()
        }
    }
    
    
    
    // MARK: - setupXIB
    func setupXIB(){
        self.tableView.register(UINib(nibName: "HeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "HeaderTableViewCell")
        self.tableView.register(UINib(nibName: "LocationTableViewCell", bundle: nil), forCellReuseIdentifier: "LocationTableViewCell")
        self.tableView.register(UINib(nibName: "CustomTableViewCell", bundle: nil), forCellReuseIdentifier: "CustomTableViewCell")
        self.tableView.separatorStyle = .singleLine
    }
   
}

   // MARK: - UITableViewDelegate,UITableViewDataSource
extension HomeviewController : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            if self.locationModelArray.count > 0 {
                return  self.locationModelArray.count
            }
         return 0
        case 1:
            if self.deviceInfoModelArray.count > 0 {
                return  self.deviceInfoModelArray.count
            }
            return 0
        default:
            return  0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        let aes256 = AES(key: self.key256, iv: self.iv)
        if indexPath.section == 0 {
        let locationInfoCell = tableView.dequeueReusableCell(withIdentifier: "LocationTableViewCell", for: indexPath) as? LocationTableViewCell
          locationInfoCell?.dateLBL.text = aes256?.decrypt(data: self.locationModelArray[indexPath.row].date)
          locationInfoCell?.latitudeLBL.text = aes256?.decrypt(data: self.locationModelArray[indexPath.row].latitude)
          locationInfoCell?.longitudeLBL.text =  aes256?.decrypt(data:self.locationModelArray[indexPath.row].longitude)
            return locationInfoCell!
        }else if indexPath.section == 1{
             let deviceInfoCell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell", for: indexPath) as? CustomTableViewCell
            deviceInfoCell?.dateLBL.text = aes256?.decrypt(data:self.deviceInfoModelArray[indexPath.row].date)
            deviceInfoCell?.cpuLBL.text = aes256?.decrypt(data: self.deviceInfoModelArray[indexPath.row].cpuUsage)
            deviceInfoCell?.memoryLBL.text = aes256?.decrypt(data: self.deviceInfoModelArray[indexPath.row].memoryUsage)
            deviceInfoCell?.batterLevelLBL.text = aes256?.decrypt(data:  self.deviceInfoModelArray[indexPath.row].batteryLevel)
            deviceInfoCell?.batteyStateLBL.text = aes256?.decrypt(data:  self.deviceInfoModelArray[indexPath.row].batteryState)
              deviceInfoCell?.batteryPerLBL.text = aes256?.decrypt(data:  self.deviceInfoModelArray[indexPath.row].batteryPercentage)
            
            return deviceInfoCell!
        }else{
           return UITableViewCell()
        }
       
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell") as! HeaderTableViewCell
        switch section {
        case 0:
            headerCell.infoHeaderLBL.text = self.headertitleArray[section]
            return headerCell.contentView
        case 1:
            headerCell.infoHeaderLBL.text = self.headertitleArray[section]
            return headerCell.contentView
        default:
            return UIView()
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
          return 90
        }else if indexPath.section == 1{
            return 191
        }else{
            return 0
        }
    }
}

