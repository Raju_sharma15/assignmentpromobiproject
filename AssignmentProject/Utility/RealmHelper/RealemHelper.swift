
import RealmSwift
/// helper for the realm queries
class RealmHelper {
    
  static  func editObjects(objs: Object,completion : ((_ status : Bool? )->())) {
     do{
        let realm = try Realm()
         try? realm.write ({
        realm.add(objs, update: true)
        completion(true)
        })
    }
     catch{
        print("Fetch error : \(error.localizedDescription)")
    }
    }

    static func objects<T: Object>(_ type: T.Type) -> Results<T>? {
        do{
            let realm = try Realm()
            return  realm.objects(type)
        }
        catch{
            print("Fetch error : \(error.localizedDescription)")
            
        }
        return nil
    }

    static func isObjectExist<T: Object>(id: String, type : T.Type) -> Bool {
        do{
            let realm = try Realm()
            return  realm.object(ofType: type, forPrimaryKey: id) != nil
        }
        catch{
            print("Fetch error : \(error.localizedDescription)")
            
        }
        return false
    }

    static func queryForObjects<T: Object>(type: T.Type, query: String) -> Results<T>?{
        do{
            let realm = try Realm()
            return  realm.objects(type).filter(query)
        }
        catch{
            print("Query error : \(error.localizedDescription)")
        }
        return nil
    }

    static func deleteAllObjects<T: Object>(type : T.Type){
        do{
            let realm = try Realm()
            let allObjects = realm.objects(type)
            try realm.write{
                realm.delete(allObjects)
            }
        }
        catch{
            print("Delete error : \(error.localizedDescription)")
        }
    }
  
}
