//
//  BatteryInfoClass.swift
//  AssignmentProject
//
//  Created by webwerks on 09/07/19.
//  Copyright © 2019 webwerks. All rights reserved.
//

import UIKit

class BatteryInfoClass  {
    
  static let SharedInstancebattery = BatteryInfoClass()
    var batteryState: UIDevice.BatteryState {
        return UIDevice.current.batteryState
    }
    var batteryStateClouser : ((_ batteryState : UIDevice.BatteryState  )->Void)? = nil
    var batteryLevelClouser : ((_ batteryLevel : Float  )->Void)? = nil
    var batteryPercentageClouser : ((_ currentPercentage : Int)->Void)? = nil
    init() {
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - startBatteryInfo
    
    func startBatteryInfo(){
        UIDevice.current.isBatteryMonitoringEnabled = true
        let batteryLevel = UIDevice.current.batteryLevel
        print(batteryLevel)
        NotificationCenter.default.addObserver(self, selector: #selector(batteryStateDidChange), name: UIDevice.batteryStateDidChangeNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(batteryLevelDidChange), name: UIDevice.batteryLevelDidChangeNotification, object: nil)
    }
    

    
    // MARK: - batteryStateDidChange

    @objc func batteryStateDidChange(_ notification: Notification) {
        switch batteryState {
        case .unplugged, .unknown:
            print("not charging")
        case .charging:
            print("charging or full")
        case .full :
            print("Full chnagred")
        }
        self.batteryStateClouser!(UIDevice.BatteryState(rawValue: batteryState.rawValue)!)
    }
    
    // MARK: - batteryLevelDidChange
    
    @objc func batteryLevelDidChange(notification: NSNotification) {
        let batteryLevel = UIDevice.current.batteryLevel
        if batteryLevel < 0.0 {
            print(" -1.0 means battery state is UIDeviceBatteryStateUnknown")
            return
        }
        print("Battery Level : \(batteryLevel * 100)%")
        self.batteryLevelClouser!(batteryLevel * 100)
    }
}
