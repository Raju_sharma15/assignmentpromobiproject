//
//  DeviceInfoViewModel.swift
//  AssignmentProject
//
//  Created by webwerks on 10/07/19.
//  Copyright © 2019 webwerks. All rights reserved.
//

import UIKit
import CoreLocation

class DeviceInfoViewModel {
    
    var updateTimer: Timer!
    var locationdateTimer: Timer!
    var currentLocation : CLLocation!
    var locationModelArray = [LocationInfoModel]()
    var deviceInfoModelArray = [DeviceInfoModel]()
    var locationArray : ((_ locationArray : [LocationInfoModel])->Void)? = nil
    var deviceArray : ((_ locationArray : [DeviceInfoModel])->Void)? = nil
    var key256   = "12345678901234561234567890123456"   // 32 bytes for AES256
    var iv       = "abcdefghijklmnop"
    
    init() {
           NotificationCenter.default.addObserver(self, selector: #selector(self.applicationEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    // MARK: - intializeMethod
    
    func intializeMethod(){
    
        BackgroundLocationManager.instance.start()
        let locationMgr  = BackgroundLocationManager.instance
        locationMgr.locationClouser = { location in
            let aes256 = AES(key: self.key256, iv: self.iv)
            let locationOBJ = LocationInfoModel()
            self.currentLocation = location
            print("Latitude : \(self.currentLocation.coordinate.latitude)")
            print("Longitude : \(self.currentLocation.coordinate.longitude)")
            locationOBJ.latitude =  aes256?.encrypt(string: String("\(self.currentLocation.coordinate.latitude)" ))
            locationOBJ.longitude = aes256?.encrypt(string: String("\(self.currentLocation.coordinate.longitude)" ))
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm"
            formatter.string(from: date)
            locationOBJ.date = aes256?.encrypt(string:   formatter.string(from: date))
            locationOBJ.saveobject { (true) in
                self.gettingRecordMethod()
                self.locationArray!(self.locationModelArray)
            }
        }
        updateTimer = Timer.scheduledTimer(timeInterval: 600, target: self, selector: #selector(cpuUsageAndBatteryMethod), userInfo: nil, repeats: true)
            self.gettingRecordMethod()
    }
    
    @objc func applicationEnterBackground(){
        updateTimer = Timer.scheduledTimer(timeInterval: 600, target: self, selector: #selector(cpuUsageAndBatteryMethod), userInfo: nil, repeats: true)
    }
    
    
    // MARK: - gettingRecordMethod
    
    func gettingRecordMethod(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.locationModelArray = (RealmHelper.objects(LocationInfoModel.self)?.toArray(ofType: LocationInfoModel.self))!
           if  self.locationModelArray.count > 0 {
                self.locationArray!(self.locationModelArray)
            }
            
            let deviceInfoArray = (RealmHelper.objects(DeviceInfoModel.self)?.toArray(ofType: DeviceInfoModel.self))!
            if deviceInfoArray.count > 0 {
                let lastRecordIndex = (deviceInfoArray.count - 1)
                self.deviceInfoModelArray = [deviceInfoArray[lastRecordIndex]]
                self.deviceArray!(self.deviceInfoModelArray)
            }
        }
    }
    
    // MARK: - cpuUsageAndBatteryMethod
    
    @objc func cpuUsageAndBatteryMethod() {
        let deviceOBJ  = DeviceInfoModel()
        let aes256 = AES(key: key256, iv: iv)
        let usage  =    CpuInfoClass.Sharedinstance.cpuUsage()
        deviceOBJ.cpuUsage = aes256?.encrypt(string: String(usage) + "%")
        
        let memoryUsage =   CpuInfoClass.Sharedinstance.memoryUsage()
        deviceOBJ.memoryUsage = aes256?.encrypt(string: String(memoryUsage ))
        
        let batteryInfo = BatteryInfoClass.SharedInstancebattery
        
        batteryInfo.batteryLevelClouser = { batteryLevel in
            deviceOBJ.batteryLevel = aes256?.encrypt(string: String(batteryLevel ))
        }
        
        batteryInfo.batteryStateClouser = { batteryState in
            deviceOBJ.batteryState = aes256?.encrypt(string: String(batteryState.rawValue ))
        }
        
        deviceOBJ.batteryPercentage = aes256?.encrypt(string: String(Int(UIDevice.current.batteryLevel * 100)) + "%")
        
        batteryInfo.startBatteryInfo()
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm"
        formatter.string(from: date)
        deviceOBJ.date = aes256?.encrypt(string:   formatter.string(from: date))
        deviceOBJ.saveobject { (true) in
            self.gettingRecordMethod()
            self.deviceArray!(self.deviceInfoModelArray)
        }
    }
}
